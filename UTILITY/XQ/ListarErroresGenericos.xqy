xquery version "2004-draft" encoding "Cp1252";
		<EQUIVALENCIAS_ERROR>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SVC</CATEGORIA>
			  <CODIGO>1000</CODIGO>
			  <MENSAJE>Generic Client Error</MENSAJE>
			  <DETALLE>Error gen�rico en la petici�n.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SVC</CATEGORIA>
			  <CODIGO>1001</CODIGO>
			  <MENSAJE>Missing Mandatory Parameter</MENSAJE>
			  <DETALLE>La petici�n recibida en el servicio no incluye el valor de alg�n par�metro requerido.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SVC</CATEGORIA>
			  <CODIGO>1002</CODIGO>
			  <MENSAJE>XML Body not well-formed</MENSAJE>
			  <DETALLE>El cuerpo del mensaje SOAP no est� correctamente formado.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SVC</CATEGORIA>
			  <CODIGO>1003</CODIGO>
			  <MENSAJE>XML Header not well-formed</MENSAJE>
			  <DETALLE>La cabecera del mensaje SOAP no est� correctamente formado.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SVC</CATEGORIA>
			  <CODIGO>1004</CODIGO>
			  <MENSAJE>Invalid Message</MENSAJE>
			  <DETALLE>El mensaje de respuesta del proveedor tiene un tama�o incorrecto.</DETALLE>
		   </EQUIVALENCIA>
		    <EQUIVALENCIA>
              <CATEGORIA>SVC</CATEGORIA>
              <CODIGO>1005</CODIGO>
              <MENSAJE>XML Body Response Provider not well-formed</MENSAJE>
              <DETALLE>El mensaje de respuesta del proveedor es incorrecto.</DETALLE>
           </EQUIVALENCIA>
		    <EQUIVALENCIA>
              <CATEGORIA>SVC</CATEGORIA>
              <CODIGO>1006</CODIGO>
              <MENSAJE>XML Body Response Client not well-formed</MENSAJE>
              <DETALLE>El mensaje de respuesta al cliente es incorrecto.</DETALLE>
           </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SVC</CATEGORIA>
			  <CODIGO>1050</CODIGO>
			  <MENSAJE>Business Error</MENSAJE>
			  <DETALLE>Error de negocio producido en la l�gica del servicio invocado.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SRV</CATEGORIA>
			  <CODIGO>4000</CODIGO>
			  <MENSAJE>Generic Server Fault</MENSAJE>
			  <DETALLE>Error no controlado en el proveedor de servicios.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SRV</CATEGORIA>
			  <CODIGO>4001</CODIGO>
			  <MENSAJE>Not implemented Operation</MENSAJE>
			  <DETALLE>La operaci�n solicitada mediante el SOAPAction no ha sido implementada en el servidor.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SRV</CATEGORIA>
			  <CODIGO>4002</CODIGO>
			  <MENSAJE>Service Unavailable Provider</MENSAJE>
			  <DETALLE>Hubo un problema en el lado del servidor. No puede procesar la solicitud debido a una sobrecarga temporal o mantenimiento del servidor.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SRV</CATEGORIA>
			  <CODIGO>4003</CODIGO>
			  <MENSAJE>Timeout Provider</MENSAJE>
			  <DETALLE>El proveedor no responde.</DETALLE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
              <CATEGORIA>SRV</CATEGORIA>
              <CODIGO>4004</CODIGO>
              <MENSAJE>Ocurrio un error al rutear la petici�n al proxy especifico</MENSAJE>
              <DETALLE>Error en ruteo din�mico.</DETALLE>
           </EQUIVALENCIA>
		   <EQUIVALENCIA>
		   	  <CATEGORIA>SRV</CATEGORIA>
			  <CODIGO>4050</CODIGO>
			  <MENSAJE>Technical Error</MENSAJE>
			  <DETALLE>Error t�cnico producido en el servidor durante el procesamiento del mensaje.</DETALLE>
		   </EQUIVALENCIA>
		</EQUIVALENCIAS_ERROR>