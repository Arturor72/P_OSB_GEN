xquery version "2004-draft" encoding "Cp1252";
(:: pragma  parameter="$fault" type="xs:anyType" ::)
(:: pragma  parameter="$eqErrorTecnico" type="xs:anyType" ::)
(:: pragma  parameter="$eqErrorNegocio" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/P_OSB_GEN/UTILITY/XQ/generarMsjSoapFaultRes/";
declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:generarMsjSoapFaultRes($fault as element(*),
    $eqError as element(*),
    $eqErrorTecnico as element(*),
    $errorCodigoNegocio as xs:string,
    $msjError as xs:string,
    $eqErrorNegocio as element(*))
    as element(*) {
    
       let $categoria := $eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/CATEGORIA/text()
	return

    if ($categoria='SVC')
    then
		<soapenv:Fault>
         <faultcode>soapenv:Client</faultcode>
         <faultstring>Client Exception</faultstring>
         <detail>
            <fault:ClientException xmlns:v1="http://www.telefonica.com/schemas/UNICA/SOAP/common/v1" xmlns:fault="http://www.telefonica.com/wsdl/UNICA/SOAP/common/v1/faults">
               <v1:exceptionCategory>{$eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/CATEGORIA/text()}</v1:exceptionCategory>
               <v1:exceptionCode>{fn:data($fault/ctx:errorCode)}</v1:exceptionCode>
               <v1:exceptionMsg>{$eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/MENSAJE/text()}</v1:exceptionMsg>
			   <v1:exceptionDetail>{if (fn:data($fault/ctx:errorCode)=1002 or fn:data($fault/ctx:errorCode)=1003) then $msjError else $eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/DETALLE/text()}</v1:exceptionDetail>
               <v1:exceptionSeverity>E</v1:exceptionSeverity>
					{if (fn:data($fault/ctx:errorCode)='1050') then
               		<v1:appDetail>
                  		<v1:exceptionAppCode>{$errorCodigoNegocio}</v1:exceptionAppCode>
                  		<v1:exceptionAppMessage>{$eqErrorNegocio/EQUIVALENCIA[CODIGO=fn:data($errorCodigoNegocio)]/MENSAJE/text()}</v1:exceptionAppMessage>
               		</v1:appDetail>
               		else 
               		()}
            </fault:ClientException>
         </detail>
      </soapenv:Fault>
      else 
      <soapenv:Fault>
         <faultcode>soapenv:Server</faultcode>
         <faultstring>Server Exception</faultstring>
         <detail>
            <fault:ServerException xmlns:v1="http://www.telefonica.com/schemas/UNICA/SOAP/common/v1" xmlns:fault="http://www.telefonica.com/wsdl/UNICA/SOAP/common/v1/faults">
               <v1:exceptionCategory>{$eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/CATEGORIA/text()}</v1:exceptionCategory>
               <v1:exceptionCode>{fn:data($fault/ctx:errorCode)}</v1:exceptionCode>
               <v1:exceptionMsg>{$eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/MENSAJE/text()}</v1:exceptionMsg>
               <v1:exceptionDetail>{if ($eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/DETALLE/text() != '') then $eqError/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:errorCode)]/DETALLE/text() else fn:data($fault/ctx:reason)}</v1:exceptionDetail>
               <v1:exceptionSeverity>E</v1:exceptionSeverity>
					{if (fn:data($fault/ctx:errorCode)='4050') then
               		<v1:appDetail>
                  		<v1:exceptionAppCode>{fn:data($fault/ctx:reason)}</v1:exceptionAppCode>
                  		<v1:exceptionAppMessage>{$eqErrorTecnico/EQUIVALENCIA[CODIGO=fn:data($fault/ctx:reason)]/MENSAJE/text()}</v1:exceptionAppMessage>
               		</v1:appDetail>
               		else 
					()}
            </fault:ServerException>
         </detail>
      </soapenv:Fault>
};

declare variable $fault as element(*) external;
declare variable $eqError as element(*) external;
declare variable $eqErrorTecnico as element(*) external;
declare variable $errorCodigoNegocio as xs:string external;
declare variable $msjError as xs:string external;
declare variable $eqErrorNegocio as element(*) external;

xf:generarMsjSoapFaultRes($fault,
    $eqError,
    $eqErrorTecnico,
    $errorCodigoNegocio,
    $msjError,
    $eqErrorNegocio)