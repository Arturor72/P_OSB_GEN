xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$headerIn1" element="ns0:HeaderIn" location="../../INTERFACES/INTERNAL/COMMON/API_SOAP_common_header.xsd" ::)
(:: pragma bea:global-element-return element="ns0:HeaderOut" location="../../INTERFACES/INTERNAL/COMMON/API_SOAP_common_header.xsd" ::)

declare namespace ns0 = "http://telefonica.com/globalIntegration/header";
declare namespace xf = "http://tempuri.org/P_OSB_GEN/UTILITY/XQ/PrepararRespuestaHeader/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:PrepararRespuestaHeader($tipoMsj as xs:string,
    $headerIn1 as element(ns0:HeaderIn))
    as element(soap-env:Header) {
    <soap-env:Header>
        <ns0:HeaderOut>
            <ns0:originator>{ data($headerIn1/ns0:destination) }</ns0:originator>
            <ns0:destination>{ data($headerIn1/ns0:originator) }</ns0:destination>
            {for $pid in $headerIn1/ns0:pid
                return
                <ns0:pid>{ data($pid) }</ns0:pid>
            }
            <ns0:execId>{ data($headerIn1/ns0:execId) }</ns0:execId>
            {for $msgId in $headerIn1/ns0:msgId
                return
                <ns0:msgId>{ data($msgId) }</ns0:msgId>
            }
            <ns0:timestamp>{fn:current-dateTime()}</ns0:timestamp>
            <ns0:msgType>{$tipoMsj}</ns0:msgType>
            {for $varArg in $headerIn1/ns0:varArg
                return
                <ns0:varArg>{ $varArg/@* , $varArg/node() }</ns0:varArg>
            }
        </ns0:HeaderOut>
       </soap-env:Header>
};

declare variable $tipoMsj as xs:string external;
declare variable $headerIn1 as element(ns0:HeaderIn) external;

xf:PrepararRespuestaHeader($tipoMsj,
    $headerIn1)