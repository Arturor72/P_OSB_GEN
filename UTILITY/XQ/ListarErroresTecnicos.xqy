xquery version "2004-draft" encoding "Cp1252";
		<EQUIVALENCIAS_TECNICO>
		   <EQUIVALENCIA>
			  <CODIGO>5001</CODIGO>
			  <MENSAJE>El servicio no tiene parámetros</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5002</CODIGO>
			  <MENSAJE>Error en la adecuación de la auditoria 10</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5003</CODIGO>
			  <MENSAJE>Error en la adecuación de la auditoria 20</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5004</CODIGO>
			  <MENSAJE>Error en la adecuación de la auditoria 30</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5005</CODIGO>
			  <MENSAJE>Error en la adecuación de la auditoria 40</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5006</CODIGO>
			  <MENSAJE>Error en la adecuación del mensaje hacia el proveedor</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5007</CODIGO>
			  <MENSAJE>Error en la adecuación del mensaje hacia el consumidor</MENSAJE>
		   </EQUIVALENCIA>
		   <EQUIVALENCIA>
			  <CODIGO>5008</CODIGO>
			  <MENSAJE>El servicio no tiene asignado un identificador</MENSAJE>
		   </EQUIVALENCIA>
		</EQUIVALENCIAS_TECNICO>