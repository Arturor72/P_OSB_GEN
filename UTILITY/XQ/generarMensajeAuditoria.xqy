xquery version "2004-draft" encoding "Cp1252";
(:: pragma bea:global-element-return element="int:MENSAJEREQ" location="../../INTERFACES/INTERNAL/WSDL/INTOSBAuditoria.xsd" ::)

declare namespace xf = "http://tempuri.org/P_OSB_GEN/XQ/generarMensajeAuditoria/";
declare namespace syn = "http://synopsis.integracion.osb.telefonica.com";

declare function xf:generarMensajeAditoria(	$audita as xs:string,
											$msgId as xs:string,  
											$fechaHora as xs:string,
											$hora as xs:string,
											$indOrigen as xs:string,
											$codServicio as xs:string,
											$nombreServicio as xs:string,
											$protocoloEntrada as xs:string,
											$rutaServicio as xs:string,
											$grupoServicio as xs:string,
											$anoMes as xs:string,
											$tipoEvento as xs:string,
											$tipoMensaje as xs:string,
											$osb as xs:string,
											$proxy as xs:string,
											$dato as xs:string,
											$indTransaccion as xs:string,
											$indServicio as xs:string,
											$indCliente as xs:string,
											$indExternoServicio as xs:string,
											$nroCuenta as xs:string,
											$fechaHoraOrigen as xs:string,
											$codInformativo1 as xs:string,
											$codInformativo2 as xs:string,
											$codInformativo3 as xs:string,
											$codInformativo4 as xs:string,
											$to as xs:string,
											$cc as xs:string,
											$bcc as xs:string,
											$subject as xs:string,
											$from as xs:string) as  element(syn:MENSAJEREQ)  {

 
      <syn:MENSAJEREQ>
         <INTEGREQ>
            <AUDITORIA>
               <AUDITA>{$audita}</AUDITA>
               <MSGID>{$msgId}</MSGID>
               <FECHAHORA>{$fechaHora}</FECHAHORA>
               <HORA>{$hora}</HORA>
               {if (fn:string-length($indOrigen)>30) then
               <INDORIGEN>{fn:substring($indOrigen,0,31)}</INDORIGEN>
               else
               <INDORIGEN>{$indOrigen}</INDORIGEN>
               }
               <CODSERVICIO>{$codServicio}</CODSERVICIO>
               <NOMBRESERVICIO>{$nombreServicio}</NOMBRESERVICIO>
               <PROTOCOLOENTRADA>{osbW1:asegurarDato($protocoloEntrada,'00')}</PROTOCOLOENTRADA>
               <RUTASERVICIO>{$rutaServicio}</RUTASERVICIO>
               
               {if (fn:contains($grupoServicio,'-')) then               
               <GRUPOSERVICIO>{substring-before($grupoServicio, '-')}</GRUPOSERVICIO>
               else
               <GRUPOSERVICIO>{$grupoServicio}</GRUPOSERVICIO>
               }   
               
               {if (fn:contains($grupoServicio,'-')) then               
               <TIPOSERVICIO>{substring-after($grupoServicio, '-')}</TIPOSERVICIO>
               else
               <TIPOSERVICIO>{'TFIJ'}</TIPOSERVICIO>
               }
               
               <ANOMES>{$anoMes}</ANOMES>
               <TIPOEVENTO>{$tipoEvento}</TIPOEVENTO>
               <TIPOMENSAJE>{$tipoMensaje}</TIPOMENSAJE>
               <OSB>{$osb}</OSB>
               <FLUJO>{$proxy}</FLUJO>
               <DATO>{$dato}</DATO>
               <DATOSCORREO>
               		<TO>{$to}</TO>
					<CC>{$cc}</CC>
					<BCC>{$bcc}</BCC>
					<SUBJECT>{$subject}</SUBJECT>
					<FROM>{$from}</FROM>
               </DATOSCORREO>
            </AUDITORIA>
            <TRANSACCION>
               <INDTRANSACCION>{$indTransaccion}</INDTRANSACCION>
               <INDSERVICIO>{$indServicio}</INDSERVICIO>
               <INDCLIENTE>{$indCliente}</INDCLIENTE>
               <INDEXTERNOSERVICIO>{$indExternoServicio}</INDEXTERNOSERVICIO>
               <NROCUENTA>{$nroCuenta}</NROCUENTA>
               <FECHAHORAORIGEN>{$fechaHoraOrigen}</FECHAHORAORIGEN>
               <CODINFORMATIVO1>{$codInformativo1}</CODINFORMATIVO1>
               <CODINFORMATIVO2>{$codInformativo2}</CODINFORMATIVO2>
               <CODINFORMATIVO3>{$codInformativo3}</CODINFORMATIVO3>
               <CODINFORMATIVO4>{$codInformativo4}</CODINFORMATIVO4>
            </TRANSACCION>
         </INTEGREQ>
      </syn:MENSAJEREQ>


};

declare variable $audita as xs:string external;
declare variable $msgId as xs:string external;
declare variable $fechaHora as xs:string external;
declare variable $hora as xs:string external;
declare variable $indOrigen as xs:string external;
declare variable $codServicio as xs:string external;
declare variable $nombreServicio as xs:string external;
declare variable $protocoloEntrada as xs:string external;
declare variable $rutaServicio as xs:string external;
declare variable $grupoServicio as xs:string external;
declare variable $anoMes as xs:string external;
declare variable $tipoEvento as xs:string external;
declare variable $tipoMensaje as xs:string external;
declare variable $osb as xs:string external;
declare variable $proxy as xs:string external;
declare variable $dato as xs:string external;
declare variable $indTransaccion as xs:string external;
declare variable $indServicio as xs:string external;
declare variable $indCliente as xs:string external;
declare variable $indExternoServicio as xs:string external;
declare variable $nroCuenta as xs:string external;
declare variable $fechaHoraOrigen as xs:string external;
declare variable $codInformativo1 as xs:string external;
declare variable $codInformativo2 as xs:string external;
declare variable $codInformativo3 as xs:string external;
declare variable $codInformativo4 as xs:string external;
declare variable $to as xs:string external;
declare variable $cc as xs:string external;
declare variable $bcc as xs:string external;
declare variable $subject as xs:string external;
declare variable $from as xs:string external;

xf:generarMensajeAditoria(	$audita,  
							$msgId,
							$fechaHora,
							$hora,
							$indOrigen,
							$codServicio,
							$nombreServicio,
							$protocoloEntrada,
							$rutaServicio,
							$grupoServicio,
							$anoMes,
							$tipoEvento,
							$tipoMensaje,
							$osb,
							$proxy,
							$dato,
							$indTransaccion,
							$indServicio,
							$indCliente,
							$indExternoServicio,
							$nroCuenta,
							$fechaHoraOrigen,
							$codInformativo1,
							$codInformativo2,
							$codInformativo3,
							$codInformativo4,
							$to,
							$cc,
							$bcc,
							$subject,
							$from)